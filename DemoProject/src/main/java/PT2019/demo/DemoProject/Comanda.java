package PT2019.demo.DemoProject;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class Comanda implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JFrame frame = new JFrame();
	public static int nr_comanda = 0;
	static JTextField t1 = new JTextField("");
	static JTextField t2 = new JTextField("");
	static JTextField t3 = new JTextField("");
	static JTextField t4 = new JTextField("");
	static JTextField t = new JTextField("");
	public static Lista l = new Lista();
	public static Lista1 li = new Lista1();
	public static int fac = 0;

	public static void init() {

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		int k = -1;
		final Lista l = new Lista();
		final Lista1 li = new Lista1();
		final Serializare s = new Serializare();
		JPanel p1 = new JPanel();
		JButton b1 = new JButton("Creaza");
		JButton b2 = new JButton("Vizualizeaza");
		JButton b3 = new JButton("Inapoi");
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFrame frame1 = new JFrame();
				frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame1.setSize(500, 200);
				JPanel p = new JPanel();
				JPanel p1 = new JPanel();
				JPanel p3 = new JPanel();
				p.setLayout(new GridLayout(4, 2));
				JLabel l1 = new JLabel("Nume:");
				JButton b = new JButton("Adauga");
				JButton b1 = new JButton("Inapoi");
				JButton b2 = new JButton("Scrie");
				p.add(l1);
				p.add(t1);
				p1.add(b);
				p1.add(b1);
				p1.add(b2);
				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String nume;
						nume = t1.getText();
						int k = -1;
						for (int i = 0; i < Administrator.numar_meniu; i++) {
							if (Administrator.menu[i].nume.equals(nume))
								k = i;
						}
						if (k == -1)
							System.out.println("Nu");
						else {
							l.add(Administrator.menu[k]);
						}

					}
				});
				b2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						li.add(l);
						String nume;
						nume = t1.getText();
						s.scrie("Comanda" + nr_comanda + ".txt", li);
						JOptionPane.showMessageDialog(null, "Comanda realizata");
						Writer writer = null;
						fac++;
						try {
							writer = new BufferedWriter(
									new OutputStreamWriter(new FileOutputStream("Factura" + fac + ".txt"), "utf-8"));
							for (int i = 0; i < li.x; i++) {
								for (int j = 0; j < li.menu[i].x; j++) {
									writer.write("Nume:" + li.menu[i].menu[j].nume);
									((BufferedWriter) writer).newLine();
									writer.write("Pret_total=" + li.menu[i].menu[j].pret_total);
									writer.write("");
								}
							}
						} catch (IOException ex) {
							// Report
						} finally {
							try {
								writer.close();
							} catch (Exception ex) {
								/* ignore */}
						}
						for (int i = 0; i < l.x; i++)
							l.menu[i].dellProd(l.menu[i].nume);
						l.x = 0;
					}
				});
				b1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(true);
						frame1.setVisible(false);
						nr_comanda++;
					}
				});
				p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
				p3.add(p);
				p3.add(p1);
				frame1.add(p3);
				frame1.setVisible(true);
				frame.setVisible(false);

			}
		});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JPanel p = new JPanel();
				String nume = "Comanda.txt";
				if (nr_comanda == 0)
					nr_comanda = 2;
				// Lista1 c1=new Lista1();
				// c1=(Lista1) s.citire(nume, 0);

				JScrollPane[] sp = new JScrollPane[100];
				JLabel[] lb = new JLabel[100];
				for (int i = 0; i < 100; i++) {
					lb[i] = new JLabel();
					sp[i] = new JScrollPane();
				}
				for (int i = 0; i < nr_comanda; i++) {
					nume = "Comanda" + i + ".txt";
					Lista1 c1 = new Lista1();
					c1 = (Lista1) s.citire(nume, 0);
					lb[i].setText("Numar Comanda=" + i);
					p.add(lb[i]);
					for (int j = 0; j < c1.menu[i].x; j++) {
						c1.menu[i].menu[j].afisMeniu();
						sp[i] = new JScrollPane(c1.menu[i].menu[j].jt);
						p.add(sp[i]);
					}
				}

				final JFrame frame1 = new JFrame();
				frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame1.setSize(800, 800);

				// p.setLayout(new GridLayout(4,2));
				JPanel p1 = new JPanel();
				JPanel p3 = new JPanel();
				JButton b1 = new JButton("Inapoi");
				p1.add(b1);
				b1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(true);
						frame1.setVisible(false);
					}
				});
				p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
				p3.add(p);
				p3.add(p1);
				frame1.add(p3);
				frame1.setVisible(true);
				frame.setVisible(false);

			}
		});

		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				Restaurant.frame.setVisible(true);
			}
		});
		p1.add(b1);
		p1.add(b2);
		p1.add(b3);
		frame.add(p1);
		frame.setVisible(true);
	}

}
