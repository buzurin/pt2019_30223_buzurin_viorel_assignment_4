package PT2019.demo.DemoProject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializare {
	
	
	public  void scrie(String nume,Object o)
	{
		 try { 
			  
	            // Saving of object in a file 
	            FileOutputStream file = new FileOutputStream 
	                                           (nume); 
	            ObjectOutputStream out = new ObjectOutputStream 
	                                           (file); 
	  
	            // Method for serialization of object 
	            out.writeObject(o); 
	  
	            out.close(); 
	            file.close(); 
	  
	            System.out.println("Object has been serialized\n"
	                              + "Data before Deserialization."); 	           
	        } 
		 catch (IOException ex) { 
	            System.out.println("IOException is caught"); 
	        } 
	}
	public  Object citire(String nume,int t)
	{
		Object o=new Object();
		  try { 
			  
	            // Reading the object from a file 
	            FileInputStream file = new FileInputStream 
	                                         (nume); 
	            ObjectInputStream in = new ObjectInputStream 
	                                         (file); 
	  
	            // Method for deserialization of object 
	          
	            o = (Object)in.readObject(); 
	  
	            in.close(); 
	            file.close(); 
	            System.out.println("Object has been deserialized\n"
	                                + "Data after Deserialization."); 	            
	        } 
	  
	        catch (IOException ex) { 
	            System.out.println("IOException is caught"); 
	        } 
	  
	        catch (ClassNotFoundException ex) { 
	            System.out.println("ClassNotFoundException" + 
	                                " is caught"); 
	        } 
		  return o;
	     
	}

}
