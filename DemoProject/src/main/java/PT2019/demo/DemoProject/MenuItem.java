
package PT2019.demo.DemoProject;


import java.awt.Dimension;
import java.io.Serializable;

import javax.swing.JTable;
public class MenuItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String nume;
	private String []produse=new String[100];
	private float []pret=new float[100];
	public float pret_total=0;
	private int numar_produse=0;
	public JTable jt;
	public MenuItem()
	{		
		this.pret_total=0;		
		this.numar_produse=0;
	}
	
	public MenuItem(String nume,String []p,float []pr,float prt)
	{
		this.nume=nume;
		this.pret=pr;
		this.pret_total=prt;
		this.produse=p;
		int i=0;
		while(pr.length>i)
		{
			i++;
		}
		this.numar_produse=i;
	}
	public void addProd(String nume,float pret)
	{		
		this.produse[this.numar_produse]=nume;
		this.pret[this.numar_produse]=pret;
		this.pret_total=this.pret_total+pret;
		this.numar_produse++;
	}
	public void eddProd(String nume,float pret,String vnume)
	{
		for(int i=0;i<this.numar_produse;i++)
		{
			if(this.produse[i]==vnume)
			{
				this.produse[i]=nume;
				this.pret[i]=pret;
				i=numar_produse;
			}
		}
	}
	public void dellProd(String nume)
	{
		for(int i=0;i<this.numar_produse;i++)
		{
			if(this.produse[i]==nume)
			{
				for(int j=i;j<numar_produse;j++)
				{
					this.produse[j]=this.produse[j+1];
				}
				this.numar_produse--;
				i=numar_produse;
			}
		}
	}
	public void afisMeniu()
	{
		String [][]data=new String[this.numar_produse+2][2];//            {"101","Sachin","700000"}};   
		System.out.println(this.numar_produse);
		for(int i=0;i<this.numar_produse;i++)
		{
			
			data[i][0]=this.produse[i];
			data[i][1]=""+this.pret[i];
			//this.pret_total=this.pret_total+this.pret[i];
		}
		data[this.numar_produse][0]="Pret_Total";
		data[this.numar_produse][1]=""+this.pret_total;
        String column[]={"NUME","PRET"}; 
        this.jt=new JTable(data,column);
    	jt.setPreferredScrollableViewportSize(new Dimension(200, 200));
		jt.setFillsViewportHeight(true);
	}
	
}
